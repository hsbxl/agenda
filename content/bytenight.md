---
startdate:  2019-02-02
starttime: "from 18h"
#enddate: 2019-02-02
#endtime: "05h"
linktitle: "Bytenight"
title: "ByteNight"
location: "HSBXL"
eventtype: ""
price: ""
#image: "bytenight.png"
---

## Stable version
- [Bytenight v2019]({{<ref "events/Bytenight-2019.md">}})

## Unsupported, deprecated versions
- [Bytenight v2018](https://wiki.hsbxl.be/Bytenight_2018)
- [Bytenight v2017](https://wiki.hsbxl.be/Bytenight_2017)
- [Bytenight v2016](https://wiki.hsbxl.be/Bytenight_2016)
- [Bytenight v2015](https://wiki.hsbxl.be/Bytenight_2015)
- [Bytenight v2014](https://wiki.hsbxl.be/Bytenight_2014)
- [Bytenight v2013](https://wiki.hsbxl.be/Bytenight_2013)
- [Bytenight v2012](https://wiki.hsbxl.be/Bytenight_2012)
- [Bytenight v2011](https://wiki.hsbxl.be/Bytenight_2011)
- [Bytenight v2010](https://wiki.hsbxl.be/Bytenight_2010)
