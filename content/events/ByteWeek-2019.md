---
startdate:  2019-01-26
#starttime: "13h"
enddate: 2019-02-02
#endtime: "05h"
linktitle: "ByteWeek"
title: "Byteweek"
location: "HSBXL"
eventtype: ""
price: ""
image: "byteweek.png"
---

The week before Fosdem conference, HSBXL compiles ByteWeek.  
One week of hackatons, workshops, talks, and we end the week with the famous [Bytenight]({{<ref "events/Bytenight-2019.md">}})!

## Call for participation
Send a mail to **contact@hsbxl.be** with your proposal.

### Currently on the menu
- Tuesday 29th Jan: Introduction to LDAP (Talk/Workshop)
- Wednesday 30th Jan: Stompboxin', DIY fuzzboxes for bass and guitar (workshop)
- Thursday 31st Jan: zeromq (Hackaton)
- Friday 1st Feb: zeromq (Hackaton)
- Friday 1st Feb: blockstack (Talk)
- Saturday 2nd Feb: [Bytenight]({{<ref "events/Bytenight-2019.md">}}) (Thé dansant)

More to come...