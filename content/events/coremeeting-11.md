---
startdate:  2018-11-03
starttime: "13h"
linktitle: "Coremeeting 11"
title: "Coremeeting 11"
location: "HSBXL"
eventtype: "Meeting"
price: ""
image: "coremeeting.png"
---

Our monthly CoreMeeting. Discussing main issues like the bookkeeping.
Everyone is welcome on this meeting.