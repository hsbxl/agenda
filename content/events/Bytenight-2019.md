---
startdate:  2019-02-02
starttime: "18h"

enddate: 2019-02-03
endtime: "early in the morning"

linktitle: "Bytenight"
title: "Bytenight 2019"
location: "HSBXL"
eventtype: "Thé dansant"
price: ""
image: "bytenight.png"
---

As an after-party to the big FOSDEM Free Software conference in Brussels, the oldest Hackerspace in town organises its annual free party.

## Music line up
Weird music for weird people.

* Jo 'dJc' Clarysse
* CIRC8
* Jonny and the bomb
* Open impro table in the back

## Organizing
Notes can be found on https://etherpad.openstack.org/p/bytenight2018

